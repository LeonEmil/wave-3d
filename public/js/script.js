//'use strict'

import { url } from "/settings/variables.js"
import * as THREE from "https://cdnjs.cloudflare.com/ajax/libs/three.js/r118/three.module.js"
import { OrbitControls } from "/jsm/controls/OrbitControls.js"
import { UnrealBloomPass } from "/jsm/postprocessing/UnrealBloomPass.js"
import { RenderPass } from "/jsm/postprocessing/RenderPass.js"
import { EffectComposer } from "/jsm/postprocessing/EffectComposer.js"
import { ShaderPass } from "/jsm/postprocessing/ShaderPass.js"


// Variables
let container, sceneWidth, sceneHeight, scene, renderer, camera, controls, sun, axesHelper, composer

// Functions

const init = () => {
    createScene()
    update()
}

const createScene = () => {
    sceneWidth = window.innerWidth
    sceneHeight = window.innerHeight

    let group = new THREE.Group()

    scene = new THREE.Scene()

    let skybox = new THREE.CubeTextureLoader().load(
        [
            `${url}/skybox_right.png`,
            `${url}/skybox_left.png`,
            `${url}/skybox_up.png`,
            `${url}/skybox_down.png`,
            `${url}/skybox_back.png`,
            `${url}/skybox_front.png`,
        ]
    )
    scene.background = skybox

    renderer = new THREE.WebGLRenderer({antialias: true, alpha: true})
    renderer.setPixelRatio(window.devicePixelRatio)
    renderer.setSize( sceneWidth, sceneHeight)

    container = document.getElementById("container")
    container.appendChild(renderer.domElement)

    camera = new THREE.PerspectiveCamera(30, sceneWidth / sceneHeight, 1, 10000)
    camera.position.set(0, 60, 600)

    let renderScene = new RenderPass(scene, camera)
    let bloompass = new UnrealBloomPass(
        new THREE.Vector2(sceneWidth, sceneHeight),
        1,
        0,
        0
    )
    composer = new EffectComposer(renderer)
    composer.addPass(renderScene)
    composer.addPass(bloompass)

    let light = new THREE.DirectionalLight(0xffffff)
    light.position.set(0, 0, 1)
    scene.add(light)

    let hemi = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.5)
    hemi.position.set(0, 0, 5)
    scene.add(hemi)

    controls = new OrbitControls(camera, renderer.domElement)
    controls.update()

    axesHelper = new THREE.AxesHelper(15);
    scene.add(axesHelper);
    axesHelper.position.y = 10
    
    let sunGeo = new THREE.IcosahedronGeometry(100, 1)
    sunGeo.computeFlatVertexNormals()
    let sunMaterial = new THREE.MeshPhongMaterial({color: 0x003399})
    sun = new THREE.Mesh(sunGeo, sunMaterial)
    
    const planeBlack = new THREE.PlaneGeometry(1000, 1000, 1, 1)
    planeBlack.rotateX(-Math.PI / 2)
    let blackMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 })
    let blackMesh = new THREE.Mesh(planeBlack, blackMaterial)

    sun.position.z -= 500

    group.add(blackMesh)
    group.add(sun)
    scene.add(group)

    createPlane()
}

const createPlane = () => {
    const planeGeo = new THREE.PlaneGeometry(1000, 1000, 32, 32)
    planeGeo.rotateX(-Math.PI / 2)

    planeGeo.colorsNeedUpdate = true
    
    const material = new THREE.MeshBasicMaterial({vertexColors: THREE.VertexColors, wireframe: true})
    const planeMesh = new THREE.Mesh(planeGeo, material)
    scene.add(planeMesh)
    
    let vertices = planeGeo.vertices
    for (let i = 0; i < vertices.length; i++) {
        vertices[i].y = (Math.random() < 0.2) ? Math.random()*100 : 0
        planeGeo.faces.forEach((value) => {
            const i = planeGeo.vertices[value.a]
            const j = planeGeo.vertices[value.b]
            const k = planeGeo.vertices[value.c]
    
            const max = Math.max(i.y, j.y, k.y)
    
            if(max > 1) return value.color.set(0x00ccaf)
            else value.color.set(0xff0000)
        })
    }
}


const update = () => {
    requestAnimationFrame(update)
    //renderer.domElement.clearRect(0, 0, innerWidth, innerHeight)
    render()
}

const render = () => {
    controls.update()
    camera.position.z -= 0.1
    camera.position.x += Math.cos(Math.sin(10)) / 9
    camera.lookAt(sun.position)
    renderer.render(scene, camera)
    composer.render()
}

addEventListener("resize", () => {
    sceneWidth = window.innerWidth
    sceneHeight = window.innerHeight
    renderer.setPixelRatio(window.devicePixelRatio)
    renderer.setSize(sceneWidth, sceneHeight)
    renderer.render(scene, camera)
})

init()
